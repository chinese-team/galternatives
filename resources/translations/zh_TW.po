# Traditional Chinese translation for G Alternatives.
# Copyright (C) 2003, Gustavo Noronha Silva <kov@debian.org>
# This file is distributed under the same license as the galternatives package.
# Gavin Lai <gavin09@gmail.com>, 2017.
# Yangfl <mmyangfl@gmail.com>, 2017, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: galternatives 0.92.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-11 15:31-0500\n"
"PO-Revision-Date: 2018-06-15 20:14+0800\n"
"Last-Translator: Yangfl <mmyangfl@gmail.com>\n"
"Language-Team: Chinese (traditional) <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../galternatives/app.py:28
msgid "Enable debug output"
msgstr "啟用調試輸出"

#: ../../galternatives/app.py:31
msgid "Do not try to acquire root (as normal user)"
msgstr "不要嘗試獲取 root (以普通使用者執行)"

#: ../../galternatives/app.py:34
msgid "Specify the alternatives directory"
msgstr "指定可選項(alternatives)目錄"

#: ../../galternatives/app.py:37
msgid "Specify the administrative directory"
msgstr "指定管理目錄"

#: ../../galternatives/app.py:40
msgid "Specify the log file"
msgstr "指定日誌檔案"

#: ../../galternatives/app.py:48
msgid "Testing galternatives..."
msgstr "正在測試 galternatives..."

#: ../../galternatives/app.py:57
msgid "No root privileges detected, but continuing anyway"
msgstr "沒有檢測到 root，但仍繼續"

#: ../../galternatives/app.py:64
msgid "<b><tt>pkexec</tt> required for privileged operations.</b>"
msgstr "<b>需要 <tt>pkexec</tt> 以執行特權操作。</b>"

#: ../../galternatives/app.py:68
msgid ""
"The program needs pkexec to perform privileged alternatives system "
"modifications under normal user. Unless you have modified your system to "
"explicitly allow your normal user to do so, GAlternatives will not work."
msgstr ""
"此程式需要 pkexec，以在普通使用者下執行需要特權的備選系統修改。除非您修改了您"
"的系統組態允許普通使用者這樣做，否則 GAltarnatives 將無法工作。"

#: ../../galternatives/app.py:98
msgid "Specifying more than one group not allowed"
msgstr "不允許同時指定多個組"

#: ../../galternatives/app.py:103
msgid "No such group"
msgstr "沒有這樣的組"

#: ../../galternatives/description.py:25
msgid "No description"
msgstr "無描述"

#: ../../galternatives/description.py:140
#, python-brace-format
msgid "Install option `{3}' for group `{2}'"
msgstr "為組`{2}'安裝選項`{3}'"

#: ../../galternatives/description.py:141
#, python-brace-format
msgid "Priority: {4}"
msgstr "優先級：{4}"

#: ../../galternatives/description.py:142
msgid "Slave `{}': `{}'"
msgstr "從連結`{}'：`{}'"

#: ../../galternatives/description.py:147
#, python-brace-format
msgid "Set group `{1}' to auto mode"
msgstr "將組`{1}'設為自動模式"

#: ../../galternatives/description.py:151
#, python-brace-format
msgid "Set group `{1}' to manual mode, pointed to `{2}'"
msgstr "將組`{1}'設為手動模式，指向`{2}'"

#: ../../galternatives/description.py:156
#, python-brace-format
msgid "Remove option `{2}' for group `{1}'"
msgstr "刪除組`{1}'的選項`{2}'"

#: ../../galternatives/description.py:159
#, python-brace-format
msgid "Remove group `{1}'"
msgstr "刪除組`{1}'"

#: ../../galternatives/appdata.py:89
msgid "Logo missing. Is your installation correct?"
msgstr "圖示遺失。您的安裝是否正確？"

#: ../../galternatives/__init__.py:44
msgid ""
"A tool to help the administrator select which programs provide specific "
"services for the user by default."
msgstr "幫助管理員選擇為使用者提供特定服務的預設選項的一個工具。"

#: ../../galternatives/gui.py:61
msgid "Select File"
msgstr "選擇檔案"

#: ../../galternatives/gui.py:209 ../../galternatives/gui.py:276
msgid "Name"
msgstr "名稱"

#: ../../galternatives/gui.py:210
msgid "Link"
msgstr "連結"

#: ../../galternatives/gui.py:218
msgid "Edit group - {}"
msgstr "編輯組 - {}"

#: ../../galternatives/gui.py:225
msgid "Add group"
msgstr "添加組"

#: ../../galternatives/gui.py:272 ../../galternatives/gui.py:277
msgid "Path"
msgstr "路徑"

#: ../../galternatives/gui.py:273 ../glade/galternatives.glade.h:16
msgid "Priority"
msgstr "優先級"

#: ../../galternatives/gui.py:289
msgid "Edit option - {}"
msgstr "編輯選項 - {}"

#: ../../galternatives/gui.py:296
msgid "Add option"
msgstr "添加選項"

#: ../../galternatives/gui.py:529
msgid "Run command: "
msgstr "執行命令："

#: ../../galternatives/gui.py:800
msgid "translator_credits"
msgstr "(由開發者提供)"

#: ../org.debian.galternatives.desktop.in.h:1
msgid "Alternatives Configurator"
msgstr "可選項配置工具"

#: ../org.debian.galternatives.desktop.in.h:2
msgid "Configure the system default alternatives"
msgstr "配置系統可選項的預設值"

#: ../org.debian.galternatives.desktop.in.h:3
msgid "Alternatives;GAlternatives;Debian;"
msgstr "可選項;配置;Debian;"

#: ../descriptions/x-www-browser.desktop.in.h:1
msgid "X WWW Browser"
msgstr "X 網路瀏覽器"

#: ../descriptions/x-www-browser.desktop.in.h:2
msgid "Default WWW Browser to launch URLs and browse the Internet."
msgstr "預設網路瀏覽器，用於開啟 URL 或瀏覽網際網路。"

#: ../descriptions/jar.desktop.in.h:1
msgid ""
"Creates an archive for classes and resources, and manipulate or restore "
"individual classes or resources from an archive."
msgstr ""
"建立類和資源的檔案，或處理檔案中的單個類或資源或者從檔案中還原單個類或資源。"

#: ../descriptions/cpp.desktop.in.h:1
msgid "C Preprocessor."
msgstr "C 前處理器。"

#: ../descriptions/wine.desktop.in.h:1
msgid "Compatibility layer for Microsoft Windows programs."
msgstr "Microsoft Windows 程式相容層。"

#: ../descriptions/desktop-theme.desktop.in.h:1
msgid "Desktop Theme"
msgstr "桌面主題"

#: ../descriptions/desktop-theme.desktop.in.h:2
msgid "Default desktop theme."
msgstr "預設桌面主題。"

#: ../descriptions/google-chrome.desktop.in.h:1
msgid "Google Chrome"
msgstr "Google Chrome"

#: ../descriptions/google-chrome.desktop.in.h:2
msgid "Default Release Channel of Google Chrome to use."
msgstr "要預設使用的 Google Chrome 釋出版本。"

#: ../descriptions/x-terminal-emulator.desktop.in.h:1
msgid "X Terminal Emulator"
msgstr "X 終端模擬器"

#: ../descriptions/x-terminal-emulator.desktop.in.h:2
msgid ""
"Default Terminal Emulator to use when running text-based applications, e.g. "
"from desktop-neutral mailcap entries."
msgstr ""
"預設終端模擬器，在執行基於文字的應用程式時使用，如各桌面環境通用的 mailcap "
"項。"

#: ../descriptions/desktop-background.desktop.in.h:1
msgid "Desktop Background"
msgstr "桌面背景"

#: ../descriptions/desktop-background.desktop.in.h:2
msgid "Default desktop wallpaper."
msgstr "預設桌面桌布。"

#: ../descriptions/unrar.desktop.in.h:1
msgid "Extractor for RAR archives."
msgstr "RAR 歸檔提取器。"

#: ../descriptions/editor.desktop.in.h:1
msgid "Default command line editor, usually called by `editor` command."
msgstr "預設的命令列編輯器，通常由`editor`命令呼叫。"

#: ../descriptions/x-cursor-theme.desktop.in.h:1
msgid "X Cursor Theme"
msgstr "X 游標主題"

#: ../descriptions/x-cursor-theme.desktop.in.h:2
msgid ""
"Default Cursor Theme that helps various aspects of GUI navigation and "
"manipulation."
msgstr "預設游標主題，用於 GUI 各導航和操作中。"

#: ../descriptions/gnome-www-browser.desktop.in.h:1
msgid "Gnome WWW Browser"
msgstr "Gnome 網路瀏覽器"

#: ../descriptions/gnome-www-browser.desktop.in.h:2
msgid "Default WWW Browser for GNOME Desktop Environment."
msgstr "用於 GNOME 桌面環境的預設網路瀏覽器。"

#: ../descriptions/java.desktop.in.h:1
msgid "Default Java Runtime."
msgstr "預設 Java 執行時。"

#: ../descriptions/convert.desktop.in.h:1
msgid "ImageMagick command-line processing tool."
msgstr "ImageMagick 命令列處理工具。"

#: ../org.debian.galternatives.metainfo.xml.in.h:1
#: ../glade/galternatives.glade.h:1
msgid "G Alternatives"
msgstr "G Alternatives"

#: ../org.debian.galternatives.metainfo.xml.in.h:2
msgid "Graphical setup tool for the alternatives system"
msgstr ""

#: ../org.debian.galternatives.metainfo.xml.in.h:3
#, fuzzy
msgid ""
"<p> A GUI to help the system administrator to choose what program should "
"provide a given service. </p> <p> This is a graphical front-end to the "
"update-alternatives program shipped with dpkg. </p>"
msgstr "幫助管理員選擇為使用者提供特定服務的預設選項的一個工具。"

#: ../org.debian.galternatives.metainfo.xml.in.h:4
msgid "New maintenance release with AppStream Metadata added."
msgstr ""

#: ../org.debian.galternatives.metainfo.xml.in.h:5
msgid "New maintenance release."
msgstr ""

#: ../org.debian.galternatives.metainfo.xml.in.h:6
msgid "New stable release."
msgstr ""

#: ../org.debian.galternatives.metainfo.xml.in.h:7
msgid "New stable release, add feature to search entries."
msgstr ""

#: ../glade/edit_dialog.glade.h:1
msgid "Slaves"
msgstr "從配置"

#: ../glade/edit_dialog.glade.h:2
msgid "No changes will be made until you add an option."
msgstr "添加選項之前，不會更改任何內容。"

#: ../glade/galternatives.glade.h:2
msgid "Create"
msgstr "建立"

#: ../glade/galternatives.glade.h:3
msgid "_Create"
msgstr "建立(_C)"

#: ../glade/galternatives.glade.h:4
msgid "Edit"
msgstr "編輯"

#: ../glade/galternatives.glade.h:5
msgid "_Edit"
msgstr "編輯(_E)"

#: ../glade/galternatives.glade.h:6
msgid "Remove"
msgstr "刪除"

#: ../glade/galternatives.glade.h:7
msgid "_Remove"
msgstr "刪除(_R)"

#: ../glade/galternatives.glade.h:8
msgid "Find"
msgstr "查詢"

#: ../glade/galternatives.glade.h:9
msgid "_Find"
msgstr "查詢(_F)"

#: ../glade/galternatives.glade.h:10
msgid "Select an alternative group to edit"
msgstr "選擇要編輯的可選項組"

#: ../glade/galternatives.glade.h:11
msgid "Groups"
msgstr "組"

#: ../glade/galternatives.glade.h:12
msgid "Auto:"
msgstr "自動："

#: ../glade/galternatives.glade.h:13
msgid "Auto mode"
msgstr "自動模式"

#: ../glade/galternatives.glade.h:14
msgid "Alternative"
msgstr "可選項"

#: ../glade/galternatives.glade.h:15
msgid "Choose an alternative group"
msgstr "選擇一個可選項組"

#: ../glade/galternatives.glade.h:17
msgid "Package"
msgstr "套件"

#: ../glade/galternatives.glade.h:18
msgid "Some changes are not saved."
msgstr "某些更改尚未保存。"

#: ../glade/galternatives.glade.h:19
msgid "Are you sure you want to quit now?"
msgstr "您確定現在退出嗎？"

#: ../glade/galternatives.glade.h:20
msgid "Alternative settings editing is advanced feature."
msgstr "編輯可選項設置是進階功能。"

#: ../glade/galternatives.glade.h:21
msgid ""
"Those are meant to be managed by <tt>dpkg</tt> and provided by their "
"corresponding packages.\n"
"<b>Warning!</b> This feature is not stable and may contain bugs. Manual "
"editing may break your system."
msgstr ""
"這些是由 <tt>dpkg</tt> 管理，由相應的軟件包提供。\n"
"<b>警告！</b>此功能不穩定，可能包含錯誤。手動編輯可能會破壞您的系統。"

#: ../glade/galternatives.glade.h:23
msgid "Show this dialog next time."
msgstr "下次顯示此對話框。"

#: ../glade/galternatives.glade.h:24
msgid "Preferences"
msgstr "偏好設定"

#: ../glade/galternatives.glade.h:25
msgid "See update-alternatives(1) \"OPTIONS\" section for details"
msgstr "詳細資訊參照 update-alternatives(1) “OPTIONS”章節"

#: ../glade/galternatives.glade.h:26
msgid "Paths"
msgstr "路徑"

#: ../glade/galternatives.glade.h:27
msgid "Commit failed"
msgstr "提交失敗"

#: ../glade/galternatives.glade.h:28
msgid "An error has occurred. The results are:"
msgstr "發生了錯誤。結果為："

#: ../glade/galternatives.glade.h:29
msgid "Be careful! Your system may be already in an unstable state."
msgstr "小心！您的系統可能已經處於不穩定狀態。"

#: ../glade/menubar.glade.h:1
msgid "_Delay mode"
msgstr "延遲模式(_D)"

#: ../glade/menubar.glade.h:2
msgid "Query _package (high I/O)"
msgstr "查詢軟體套件(高 I/O)(_P)"

#: ../glade/menubar.glade.h:3
msgid "_Use polkit"
msgstr "使用 polkit(_U)"

#: ../glade/menubar.glade.h:4
msgid "_Working dir..."
msgstr "工作目錄...(_W)"

#: ../glade/menubar.glade.h:5
msgid "_About"
msgstr "關於(_A)"

#: ../glade/menubar.glade.h:6
msgid "_Quit"
msgstr "退出(_Q)"

#~ msgid ""
#~ "Install option for group `{2}' with priority {4}, `{3}' for master link"
#~ msgstr "為組`{2}'安裝選項 {4}，優先級 {4}，`{3}'用於主連結"

#~ msgid ", `{}' for slave link `{}'"
#~ msgstr "，`{}'用於從連結`{}'"

#~ msgid ""
#~ "No `pkexec' detected, but found `gksudo'. You should really consider "
#~ "polkit."
#~ msgstr "沒有檢測到`pkexec'，但發現`gksudo'。您應當考慮 polkit。"

#~ msgid "Running Alternatives Configurator..."
#~ msgstr "正在執行可選項配置工具..."

#~ msgid ""
#~ "<b>I need your root password to run\n"
#~ "the Alternatives Configurator.</b>"
#~ msgstr ""
#~ "<b>我需要您的 root 密碼來執行\n"
#~ "可選項配置工具。</b>"

#~ msgid ""
#~ "<b>This program should be run as root and <tt>/usr/bin/gksu</tt> is not "
#~ "available.</b>"
#~ msgstr ""
#~ "<b>該程式應當以 root 使用者身分執行，但是 <tt>/usr/bin/gksu</tt> 不可用。"
#~ "</b>"

#~ msgid "Run update-alternatives tool to modify system alternative selections"
#~ msgstr "執行 update-alternatives 工具以修改系統可選項"

#~ msgid "Authentication is required to run update-alternatives tool"
#~ msgstr "需要驗證才能執行 update-alternatives 工具"

#~ msgid "Icon"
#~ msgstr "圖示"

#~ msgid "Messages"
#~ msgstr "消息"

#~ msgid "Unknown/None"
#~ msgstr "未知/無"

#~ msgid ""
#~ "The file or directory you selected does not exist.\n"
#~ "Please select a valid one."
#~ msgstr ""
#~ "您選擇的檔案或目錄不存在。\n"
#~ "請選擇一個有效的項目。"

#~ msgid "Are you sure you want to remove this option?"
#~ msgstr "您確定要刪除這個選項嗎？"

#~ msgid "Choice"
#~ msgstr "選擇項"

#~ msgid "Options"
#~ msgstr "可選項"

#~ msgid "Slave"
#~ msgstr "子配置"

#~ msgid "_File"
#~ msgstr "檔案(_F)"

#~ msgid "_Help"
#~ msgstr "幫助(_H)"

#~ msgid "Status:"
#~ msgstr "狀態："

#~ msgid "auto"
#~ msgstr "自動"

#~ msgid "manual"
#~ msgstr "手動"

#~ msgid "<span size=\"xx-large\" weight=\"bold\">Alternative</span>"
#~ msgstr "<span size=\"xx-large\" weight=\"bold\">可選項</span>"

#~ msgid "Details"
#~ msgstr "詳細資訊"

#~ msgid "About G Alternatives"
#~ msgstr "關於 G Alternatives"

#~ msgid "<span size=\"xx-large\" weight=\"bold\">G Alternatives</span>"
#~ msgstr "<span size=\"xx-large\" weight=\"bold\">G Alternatives</span>"

#~ msgid "C_redits"
#~ msgstr "鳴謝(_R)"

#~ msgid "G Alternatives Credits"
#~ msgstr "G Altarnatives 鳴謝資訊"

#~ msgid "Gustavo Noronha Silva <kov@debian.org>"
#~ msgstr "Gustavo Noronha Silva <kov@debian.org>"

#~ msgid "Written by"
#~ msgstr "作者"

#~ msgid "Translated by"
#~ msgstr "翻譯貢獻者"

#~ msgid "Leandro A. F. Pereira <leandro@linuxmag.com.br>"
#~ msgstr "Leandro A. F. Pereira <leandro@linuxmag.com.br>"

#~ msgid "Thanks to"
#~ msgstr "感謝"

#~ msgid "Adding option to alternative"
#~ msgstr "向可選項列表中添加選項"

#~ msgid "*"
#~ msgstr "*"

#~ msgid "_Browse..."
#~ msgstr "瀏覽(_B)..."
